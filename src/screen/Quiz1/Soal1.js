import React, { Component, useState, useEffect } from 'react'
import { Text, View } from 'react-native'

const FComponent = () => {
    const [state, setState] = useState('Jhon Doe')

    useEffect(() => {
        setTimeout(() => {
            setState('Asep')
        }, 3000)
    })
    
    return(
        <View>
            <Text>{state}</Text>
        </View>

    )
}

export default FComponent
